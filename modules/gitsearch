#!/bin/sh
# Git search module for searchmenu
# mintezpresso

. $HOME/.config/searchmenu/config
. $HOME/.local/bookmarks/git/config

bookmark="$HOME/.local/bookmarks/git/bookmark"
sitelist="$HOME/.local/bookmarks/git/sites"

fn_clip () {
    case "$(xclip -se c -o | cut -d/ -f3)" in
        codeberg.org | github.com | gitlab.com | gitea.com )
            # I think this command can be shortened with a delimiter in 1 cut command
            confirm=$(printf "Yes\nNo" | dmenu -i -p "Clone and add \"$(xclip -se c -o)\" to bookmark ?")
            [ -z "$confirm" ] && exit

            [ ! "$confirm" = "Yes" ] && dunstify "User canceled adding new repo" && exit

            dunstify "Adding \"$(xclip -se c -o)\" to bookmark"
            gitname=$(xclip -se c -o | cut -d/ -f5)
            mkdir -p $gitdir/$gitname
            git -C $gitdir/$gitname clone $(xclip -se c -o)
            printf "$gitname dir $gitdir\n" >> $bookmark

            dunstify "Added \""$(xclip -se c -o | cut -d/ -f1-4)"\" to git bookmarks"
            exit ;;

        * ) dunstify "\"$(xclip -se c -o)\" not recognized as a git link" ;;
    esac
    exit
}

fn_bookmark () {

    # yes this bookmark functions requires user to have repo locally

    pickrepo=$(cut -d ' ' -f 1 $bookmark | dmenu -i -p "Select a repo from \"$searchprompt\"")
    [ -z "$pickrepo" ] || [ ! "$(grep -wo ^$pickrepo $bookmark)" = "$pickrepo" ] && exit 1

    case $(grep $pickrepo $bookmark | cut -d ' ' -f 2) in
        dir ) repodir=$(grep $pickrepo $bookmark | cut -d ' ' -f 3)
            ;;
        command ) repodir=$(grep $pickrepo $bookmark | cut -d ' ' -f 4)
            ;;
    esac

    # count number of repo then get link
    case "$(grep url $repodir/.git/config -c)" in
        1 ) repolink=$(grep url $repodir/.git/config | cut -d '@' -f 2) ;;
        * ) repolink=$(grep url $repodir/.git/config | cut -d '@' -f 2 | dmenu -i -p "Pick a repo destination")
            [ -z "$repolink" ] && exit
    esac

    $BROWSER "https://$repolink"
}

fn_search () {

    searchtype=$(printf "Repo\nUser" | dmenu -i -p "What type of query is this")
    [ -z "$searchtype" ] && exit 1

    asksite=$(cut -d ' ' -f 1 $sitelist | dmenu -i -p "Where is \"$searchprompt\" located")
    [ -z "$asksite" ] && exit 1

    case "$searchtype" in
        "Repo" ) askuser=$(printf "Just search" | dmenu -i -p "Type the owner of \"$searchprompt\", or Just search for repo")
            [ -z "$askuser" ] && exit 1
            [ "$askuser" = "Just search" ] \
                && ($BROWSER "$(grep -w "$asksite" $sitelist | cut -d ' ' -f 2)"/search?q="$(printf "$searchprompt" | sed 's/ /+/g')" && exit 0) \
                || ($BROWSER "$(grep -w "$asksite" $sitelist | cut -d ' ' -f 2)"/"$askuser"/"$searchprompt" && exit 0) \
        ;;

        "User" ) askrepo=$(printf "Just search" | dmenu -i -p "Type a repo to search, or Just search for user \"$searchprompt\"")
            [ -z "$addrepo" ] && exit 1

            [ "$askrepo" = "Just search" ] \
                && ($BROWSER "$(grep -w "$asksite" $sitelist | cut -d ' ' -f 2)"/search?q="$(printf "$searchprompt" | sed 's/ /+/g')" && exit 0) \
                || ($BROWSER "$(grep -w "$asksite" $sitelist | cut -d ' ' -f 2)"/"$searchprompt"/"$askrepo" && exit 0)
        esac
}

searchprompt=$(printf "Bookmarks\nClipboard\nEdit" | dmenu -i -p "🍴 Git")
[ -z "$searchprompt" ] && exit

case "$searchprompt" in
    "Edit" ) $editor $bookmark 2>/dev/null && exit ;;
    "Clipboard" ) fn_clip ;;
    "Bookmarks" ) fn_bookmark ;;
    * ) fn_search ;;
esac
