#!/bin/sh
# install script for searchmenu
# mintezpresso

[ ! "$(whoami)" = "root" ] && printf "Script can only run by root\n" && exit 1

mkdir -p /usr/local/bin

chmod 755 searchmenu

for files in $(ls -A modules)
do
    chmod 755 modules/$files
    cp -f modules/$files /usr/local/bin/$files
done

mkdir -p $HOME/.local/bookmarks/ $HOME/.config/searchmenu

cp -r config/* $HOME/.config/searchmenu
cp -r user/* $HOME/.local/bookmarks/
