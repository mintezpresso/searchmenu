# Search engine and bookmark manager <img src="https://img.shields.io/badge/Contributions-welcome-violet.svg?style=flat&logo=git" alt="Contributions" />
With example config files

**EDIT CONFIG FILES TO YOUR PREFERENCE BEFORE USING, OTHERWISE THINGS WON'T WORK**

# Screenshots
Main prompt: Engine selector

![](https://i.ibb.co/CvSfFV5/first-menu.png)

Search engine examples

**Normal engines**

![](https://i.ibb.co/qmxRXx0/arch-wiki.png)

**Special engines**

* Git

![](https://i.ibb.co/4KtzJx9/git.png)

* Reddit

![](https://i.ibb.co/PFDQL6y/reddit.png)

* Youtube/Invidious/ytfzf

![](https://i.ibb.co/xHYR0n3/yt.png)

* Tor

![](https://i.ibb.co/Cwx7bHY/tor-1.png)

Clipboard

![](https://i.ibb.co/RvQxFH6/clipboard.png)

Bookmarks

![](https://i.ibb.co/HFBxGPH/bookmark.png)

### Dependencies

searchmenu and modules will not check for dependencies, so it's up to the user to make sure they are all installed

- dmenu ``git clone https://git.suckless.org/dmenu``
    - patches: [grid](https://tools.suckless.org/dmenu/patches/grid/), [gridnav](https://tools.suckless.org/dmenu/patches/gridnav/), and [fuzzymatch](https://tools.suckless.org/dmenu/patches/fuzzymatch/)

- xclip
- dunst + libnotify
- your fav text editor + terminal

# Modules + modes

- \"-b\": Bookmarks only

I highly encourage writing dmenu only modules to match searchmenu's no terminal approach.

Those scripts should also be able to function as their own script independent of searchmenu.

**first 3, with their naming scheme**
- [redditsearch](https://codeberg.org/mintezpresso/searchmenu/src/branch/main/modules/redditsearch): reddit
- [gitsearch](https://codeberg.org/mintezpresso/searchmenu/src/branch/main/modules/gitsearch): git
- [torsearch](https://codeberg.org/mintezpresso/searchmenu/src/branch/main/modules/torsearch): tor

**the rest**

- [1337x](https://codeberg.org/mintezpresso/searchmenu/src/branch/main/modules/1337x): 1337x.to
- [fbmess](https://codeberg.org/mintezpresso/searchmenu/src/branch/main/modules/fbmess): fb messenger
- [nyaa](https://codeberg.org/mintezpresso/searchmenu/src/branch/main/modules/nyaa): nyaa.si
- [ytnv](https://codeberg.org/mintezpresso/searchmenu/src/branch/main/modules/ytnv): youtube/invidious/ytfzf
- [ytsmx](https://codeberg.org/mintezpresso/searchmenu/src/branch/main/modules/ytsmx): yts.mx
- [csgostats](https://codeberg.org/mintezpresso/searchmenu/src/branch/main/modules/csgostats): csgostats.gg
- [gpt](https://codeberg.org/mintezpresso/searchmenu/src/branch/main/modules/gpt): openai chatgpt + dall-e

More will be added as time goes on

### Config files (in ./config)

Installs to ``$HOME/.config/searchmenu``

- config: configs for searchmenu and module scripts
- format: change bookmark function's dmenu format according to number of entries

### User configs (in ./user)

Installs to ``$HOME/.local/bookmarks``

- engine.conf: search engine modules to be displayed with main prompt
- links.conf: your link bookmarks

# Install
```
git clone https://codeberg.org/mintezpresso/searchmenu

cd searchmenu

sudo chmod 755 install.sh

sudo ./install.sh
```

## Editing notes

**Engine file + prompt**
- "care" means selected engine turns " " into "+"
    - Ex: Youtube: "luke smith" typed in search box will result in "luke+smith" in url)
- Untested with languages other than English. Idk if typing emoji returns correct results, but its unlikely

**(1st) Search prompt query rules**
- if it looks like a link, it will be treated as one
- if it doesn't, and is not a search engine, it will be searched using search engine link

**Bookmark prompt rules**
- Bookmark displays lines of 10 items per line if there are more than 30 entries
- Edit the "format" config file if you want a different layout
